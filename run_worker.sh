#!/bin/bash

clear
echo "Cleaning..."
rm -rf composure.egg-info/ build/ dist/
echo "Uninstalling..."
pip uninstall -y composure
echo "Building..."
python setup.py bdist_wheel > /dev/null
echo "Installing..."
pip install dist/composure*.whl
echo "Starting worker..."
composure-process -c conf/worker.yaml
