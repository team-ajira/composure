from setuptools import find_packages, setup


setup(
    name='composure',
    version='1.0.0',
    author = 'Sattvik Chakravarthy',
    author_email = 'sattvik@gmail.com',
    entry_points={'console_scripts': [
        'composure-process = composure.process.default:main',
    ]},
    packages=find_packages(),
    install_requires=[
        'msgpack-python',
        'pycrypto',
        'PyYAML',
        'aiohttp',
    ],
    include_package_data=True,
    zip_safe=False
)
