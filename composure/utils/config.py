import yaml


class Config(object):
    '''Used for loading and reading settings globally'''
    config = {}

    @classmethod
    def load(cls, path):
        '''Load config from path'''
        # TODO: Populate default config
        fd = open(path)
        cls.config.update(
            yaml.load(fd)
        )
        fd.close()

    @classmethod
    def get(cls):
        '''Returns loaded config'''
        return cls.config

    @classmethod
    def value(cls, key):
        key = key.split('.')
        c = cls.config
        for k in key:
            c = c.get(k, None)
            if c is None: return None

        return c
