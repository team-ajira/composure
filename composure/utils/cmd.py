from subprocess import Popen, PIPE

import logging


class CommandExecutor(object):
    """
    Cross-platform command executor, with status and output
    Note: Commands are expected not to take any input
    """
    def __init__(self, **context):
        self.logger = logging.getLogger('CommandExecutor')
        self.context = context

    def execute(self, *args):
        self.logger.debug('Executing cmd {%s}' % str(args))
        p = Popen(args, stdout=PIPE, stderr=PIPE)
        p.wait()
        status = (p.returncode == 0)
        out = p.stdout.read()
        self.logger.debug('Command output\n---start---\n%s\n----end----' % out.decode('ascii'))
        return status, out
