import importlib
import os
import yaml


class MessageCollection(object):
    def __init__(self, messages={}):
        self._messages = messages

    def get(self, msgid, *args, **kwargs):
        s = self._messages.get(msgid)
        return s.format(*args, **kwargs)


class MessageLoader(object):
    LANG = 'en_US'
    _MESSAGE_COLLECTIONS = {}

    @classmethod
    def init(cls):
        pass

    @classmethod
    def load(cls, pkg):
        if pkg not in cls._MESSAGE_COLLECTIONS:
            module = importlib.import_module(pkg)
            msg_file = os.path.join(
                os.path.dirname(module.__file__),
                'messages',
                cls.LANG + '.yaml'
            )
            try:
                fd = open(msg_file)
                res = yaml.load(fd)
                fd.close()
                cls._MESSAGE_COLLECTIONS[pkg] = \
                    MessageCollection(res)
            except:
                return MessageCollection({})

        return cls._MESSAGE_COLLECTIONS.get(pkg)
