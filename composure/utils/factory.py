class FactoryMetaClass(type):
    def __init__(cls, name, bases, nmspc):
        super(FactoryMetaClass, cls).__init__(name, bases, nmspc)
        if hasattr(cls, 'FACTORY_BASE'):
            cls._REGISTRY = {}

        for base in bases:
            if hasattr(base, '_REGISTRY'):
                if hasattr(cls, 'ID'):
                    base.register(cls)

    def __iter__(cls):
        return iter(cls._REGISTRY.items())


class FactoryBase(object, metaclass=FactoryMetaClass):
    @classmethod
    def register(cls, new_class):
        cls._REGISTRY[new_class.ID] = new_class
        return new_class

    @classmethod
    def get_class(cls, _id):
        return cls._REGISTRY.get(_id)

    @classmethod
    def get_keys(cls):
        return cls._REGISTRY.keys()
