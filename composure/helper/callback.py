import asyncio


class CallbackHelper(object):
    """
    Base class for classes that provides async callbacks
    Call the member 'callback_init' during initialization
    """
    def callback_init(self):
        """
        Adds member to hold the registered callbacks
        """
        self._callbacks = {}

    def register_callback(self, name, method):
        """
        Register the callback
        :param name: Name of the callback
        :param method: Callback method
        """
        self._callbacks.setdefault(name, []).append(method)

    @asyncio.coroutine
    def do_callback(self, name, *args, **kwargs):
        """
        Perform the callback
        :param name: Name of the callback
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        """
        for cb in self._callbacks.get(name, []):
            if asyncio.iscoroutinefunction(cb):
                yield from cb(*args, **kwargs)
            else:
                cb(*args, **kwargs)
