from composure.utils.factory import FactoryBase
import asyncio
import logging


class Service(FactoryBase):
    """
    Base class for services
    """
    FACTORY_BASE = True
    __logname__ = 'service.Base'

    def __init__(self, config=None, loop=None):
        self.config = config or {}
        self.loop = loop or asyncio.get_event_loop()
        self.logger = logging.getLogger(self.__logname__)
        self.__manager__ = None
        self.__id__ = None

        self.init()

    def init(self):
        """
        To be defined in derived class
        """
        raise NotImplementedError()

    @asyncio.coroutine
    def run(self):
        """
        To be defined in derived class
        """
        raise NotImplementedError()

    @asyncio.coroutine
    def deinit(self):
        """
        To be defined in derived class
        """
        raise NotImplementedError()

    @property
    def manager(self):
        return self.__manager__

    @asyncio.coroutine
    def dispatch(self, event_name, *args, **kwargs):
        """
        Dispatch event to the manager
        :param event_name: event name
        :param args: positional args to the callback
        :param kwargs: keyword args to the callback
        """
        yield from self.manager.events.dispatch(self.__id__, event_name, *args, **kwargs)


class PeriodicService(Service):
    """
    Base class for Periodic services
    """
    CONFIG_INTERVAL = 'interval'

    def init(self):
        self.interval = self.config.get(self.CONFIG_INTERVAL, 60)
        self.running = False
        self.sleep_task = None

    @asyncio.coroutine
    def do(self):
        """
        To be defined in derived class
        """
        raise NotImplementedError()

    @asyncio.coroutine
    def run(self):
        self.running = True
        while self.running:
            self.sleep_task = self.loop.create_task(asyncio.sleep(self.interval))
            yield from self.do()
            try:
                yield from self.sleep_task
            except asyncio.CancelledError:
                break

    @asyncio.coroutine
    def deinit(self):
        self.running = False
        self.sleep_task.cancel()
