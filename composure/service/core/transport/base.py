import asyncio
import logging


class Transport(object):
    """
    Base class for transports like Client, Server
    """
    __logname__ = 'transport.Base'  # Override this in the derived class

    def __init__(self, config, loop=None):
        """
        Constructor
        :param config: Config object. To be consumed in the init method implemented in the derived class
        :param loop: Asyncio loop
        """
        self.loop = loop or asyncio.get_event_loop()
        self.config = config
        self.logger = logging.getLogger(self.__logname__)
        self.init()

    def init(self):
        """
        Implement transport initialization
        """
        raise NotImplementedError()
