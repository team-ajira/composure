import asyncio

from composure.service.core.transport.base import Transport

from composure.helper.callback import CallbackHelper
from composure.service.core.transport.serde import SerDe


class Server(Transport, CallbackHelper):
    """
    Server transport. Listens and accepts connections
    """

    __logname__ = 'transport.Server'

    def init(self):
        """
        Server Initialization
        """
        self.callback_init() # Needed for CallbackHelper

        self.listen_address = (self.config['listen']['host'],
            self.config['listen']['port'])

        self.serde_factory = SerDe.get_class(self.config['serde'])

        self._serdes = []
        self._serde_tasks = []
        self.complete = False

    @asyncio.coroutine
    def _handle(self, reader, writer):
        serde = self.serde_factory(reader, writer, server=True)
        serde_task = self.loop.create_task(serde.run())

        self._serdes.append(serde)
        self._serde_tasks.append(serde_task)

        yield from self.do_callback('on_connection_made', serde)
        yield from serde_task

        # serde and serde_task must be complete here. Time to remove them!
        self._serdes.remove(serde)
        self._serde_tasks.remove(serde_task)
        # There is no need to close the serde here because, serde run
        # is either complete by themselves, or finished via deinit. In either
        # case, connection would be correctly closed internally.

        yield from self.do_callback('on_connection_lost', serde)

    @asyncio.coroutine
    def deinit(self):
        """
        Server de-initialization
        """
        self.deinited = True

        # Close all the serdes' (One for each active connection)
        for serde in self._serdes:
            yield from serde.close()

        # Wait for the tasks to complete
        for serde_task in self._serde_tasks:
            yield from serde_task

    @asyncio.coroutine
    def close(self):
        """
        Close the server
        """
        self.logger.info('Closing server...')
        self.complete = True
        self.server.close() # Stop accepting new connections

        if not self.deinited:
            yield from self.deinit()
        self.future.set_result(None)

    @asyncio.coroutine
    def run(self):
        """
        Run loop for Server
        """
        self.deinited = False
        listen_address = self.listen_address
        try:
            self.logger.info('Starting master, listening on %s' % \
                             str(self.listen_address))
            self.server = yield from asyncio.start_server(self._handle,
                listen_address[0], listen_address[1], loop=self.loop)
            self.future = asyncio.Future()
            yield from self.future
            self.logger.info('Ready to accept connections')
        except Exception as e:
            self.logger.error('Could not bind to the address')
            yield from self.close()
