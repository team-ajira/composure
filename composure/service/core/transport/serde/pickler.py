import asyncio
import pickle

from .base import SerDe


class ByteReader(object):
    def __init__(self, _bytes):
        self.bytes = _bytes
        self.count = 0

    def read(self, n=None):
        if n is None or n <= 0:
            self.bytes = b''
            self.count += len(self.bytes)
            return self.bytes

        res = self.bytes[0:n]
        self.bytes = self.bytes[n:]
        self.count += len(res)
        return res

    def readline(self):
        if b'\n' in self.bytes:
            n = self.bytes.index(b'\n') + 1
            return self.read(n)
        return b''


class PickleUnpacker(object):
    def __init__(self):
        self.data = b''

    def feed(self, data):
        self.data += data

    def get(self):
        res = []
        while len(self.data) > 0:
            try:
                br = ByteReader(self.data)
                up = pickle.Unpickler(br)
                obj = up.load()
                res.append(obj)
                self.data = self.data[br.count:]
            except pickle.UnpicklingError:
                break
        return res


class Pickle(SerDe):
    ID = 'Pickle'
    BUFFER_SIZE = 1024

    def init(self):
        self.encoding = None
        self.unpacker = PickleUnpacker()

    @asyncio.coroutine
    def feed(self, data):
        self.unpacker.feed(data)
        for message in self.unpacker.get():
            yield from self.new_message(message)

    def pack(self, message):
        return pickle.dumps(message)

    def unpack(self, message):
        return pickle.loads(message)
