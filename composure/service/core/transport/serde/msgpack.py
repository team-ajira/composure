import asyncio
import msgpack

from .base import SerDe


class MsgPack(SerDe):
    ID = 'MsgPack'

    BUFFER_SIZE = 1024

    def init(self):
        self.encoding = 'utf8'
        self.unpacker = msgpack.Unpacker()

    @asyncio.coroutine
    def feed(self, data):
        self.unpacker.feed(data)
        for message in self.unpacker:
            yield from self.new_message(
                self.postunpack(message))

    def pack(self, message):
        return msgpack.dumps(
            self.prepack(message))

    def unpack(self, message):
        return self.postunpack(
            msgpack.loads(message))

    # MsgPack is unable to differentiate between bytes and str
    # Adding markers in the beginning to indicate the difference
    # and encode/decode whenever necessary
    def prepack(self, message):
        if type(message) is dict:
            out = {}
            for k, v in message.items():
                out[self.prepack(k)] = self.prepack(v)
            return out
        elif type(message) is list:
            out = []
            for v in message:
                out.append(self.prepack(v))
            return out
        elif type(message) is str:
            return b'_str_' + message.encode(self.encoding)
        elif type(message) is bytes:
            return b'_byt_' + message
        else:
            return message

    def postunpack(self, message):
        if type(message) is dict:
            out = {}
            for k, v in message.items():
                out[self.postunpack(k)] = self.postunpack(v)
            return out
        elif type(message) is list:
            out = []
            for v in message:
                out.append(self.postunpack(v))
            return out
        elif type(message) is bytes:
            if message[:5] == b'_str_':
                return message[5:].decode(self.encoding)
            elif message[:5] ==  b'_byt_':
                return message[5:]
            else:
                raise Exception('Unexpected byte stream')
        else:
            return message
