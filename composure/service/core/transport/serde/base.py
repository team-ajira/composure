import asyncio
import logging

from composure.service.core.transport.encryptor import Encryptor
from composure.utils.factory import FactoryBase


class SerDe(FactoryBase):
    """
    Base class for Serializer/Deserializer classes
    """
    FACTORY_BASE = True
    BUFFER_SIZE = 1024

    def __init__(self, reader, writer, server=False, loop=None, **extra):
        self.__reader__ = reader
        self.__writer__ = writer
        self.__rqueue__ = asyncio.Queue()
        self.__squeue__ = asyncio.Queue()
        self.is_server = server
        self.loop = loop or asyncio.get_event_loop()
        self.encryptor = None
        self.extra = extra

        self.logger = logging.getLogger('serde.' + self.__class__.__name__)

        self.init()

        self.running = False
        self.closed = False

    def init(self):
        """
        To be implemented in derived class
        """
        raise NotImplementedError()

    def feed(self, data):
        """
        Feeds data to the deserializer. Must yield messages into new_message routine
        :param data: chunk of data
        """
        raise NotImplementedError()

    def pack(self, message):
        """
        Pack message into stream of bytes
        :param message: message to be packed
        :return: serializer bytes
        """
        raise NotImplementedError()

    def unpack(self, message):
        """
        Unpack message into python object
        :param message: serialized bytes
        :return: deserialized object
        """
        raise NotImplementedError()

    def set_session_key(self, session_key):
        """
        Set session key for encryption
        :param session_key: session key
        """
        self.session_key = session_key

    @asyncio.coroutine
    def run(self):
        rtask = self.loop.create_task(self.run_receive())
        wtask = self.loop.create_task(self.run_send())
        yield from rtask
        yield from wtask
        del rtask
        del wtask

    @asyncio.coroutine
    def new_message(self, message):
        """
        New message from deserializer
        :param message: unpacked message
        """
        self.logger.debug('Received message {: %s :}' % str(message))
        yield from self.__rqueue__.put(message)

    @asyncio.coroutine
    def run_receive(self):
        """Receive loop for the SerDe"""
        self.logger.info('Receiver loop begin')
        self.running = True
        while self.running:
            try:
                data = yield from self.__reader__.read(self.BUFFER_SIZE)
                if len(data) == 0: break
                yield from self.feed(data)
            except Exception as e:
                self.logger.error('Exception occurred {: %s :} {: %s :}' % (e.__class__.__name__, str(e)))
                break
        if not self.closed:
            self.__writer__.close()
            yield from self.__rqueue__.put(None)
            yield from self.__squeue__.put(None)
        self.logger.info('Receiver loop complete')

    @asyncio.coroutine
    def run_send(self):
        """Send loop for the SerDe"""
        self.logger.info('Sender loop begin')
        while self.running:
            message = yield from self.__squeue__.get()
            if message is None:
                break
            self.__writer__.write(
                self.pack(message)
            )
            yield from self.__writer__.drain()
        self.logger.info('Sender loop complete')

    def _pack_secure(self, message):
        if self.encryptor is None:
            self.encryptor = Encryptor(self.session_key)
        message = {
            b'_sec_': True,
            b'_pld_': self.encryptor.encrypt(self.pack(message))
        }
        return message

    def _unpack_secure(self, message):
        if self.encryptor is None:
            self.encryptor = Encryptor(self.session_key)
        message = self.encryptor.decrypt(message[b'_pld_'])
        message = self.unpack(message)
        return message

    @asyncio.coroutine
    def send(self, message, secure=False):
        """
        Send message on the transport
        :param message: message to be sent
        :param secure: if the message has to be securely sent
        """
        self.logger.debug('Sending message {: %s :}' % str(message))
        # TODO: Preprocess message
        # TODO: Add options on encoding and decoding
        if secure:
            message = self._pack_secure(message)
        yield from self.__squeue__.put(message)

    @asyncio.coroutine
    def receive(self):
        """
        Receive message from the transport
        :return: incoming message
        """
        message = yield from self.__rqueue__.get()
        # TODO: Postprocess message
        if message is None:
            return None
        if type(message) is dict and b'_sec_' in message and message[b'_sec_']:
            message = self._unpack_secure(message)
        return message

    @asyncio.coroutine
    def close(self):
        """
        Close the SerDe, and let the tasks complete
        """
        self.running = False
        self.closed = True
        self.__writer__.close()
        yield from self.__rqueue__.put(None) # Kick out the reader loop
        yield from self.__squeue__.put(None) # Kick out the sender loop
