from Crypto.Cipher import AES
from Crypto import Random


# Encryptor expects inputs to be of length that is a multiple of 16.
# Following methods help with padding and unpadding input data.
BS = 16
_pad = lambda s: s + (BS - len(s) % BS) * bytes([BS - len(s) % BS])
_unpad = lambda s : s[:-ord(s[len(s)-1:])]


class Encryptor(object):
    """
    Default encryptor for secure transports. Provides encrypt and decrypt methods
    """
    def __init__(self, key):
        """
        Constructor
        :param key: Encrypt/Decrypt key to be used. Note: If key length is not multiple of 16, it will be truncated to a length which is a multiple of 16
        """
        self.key = key

        self.rkey = ''
        while len(key) >= 16:
            self.rkey = key[:16] + self.rkey
            key = key[16:]

    def encrypt(self, inp):
        """
        Encrypts the input
        :param inp: Input bytes
        :return: Encrypted data
        """
        key = self.key
        inp = _pad(inp)
        while len(key) >= 16:
            iv = Random.new().read(AES.block_size)
            enc = AES.new(key[:16], AES.MODE_CFB, iv)
            inp = iv + enc.encrypt(inp)
            key = key[16:]
        return inp

    def decrypt(self, inp):
        """
        Decrypts the input
        :param inp: Input bytes
        :return: Decrypted data
        """
        key = self.rkey
        while len(key) >= 16:
            iv = inp[:16]
            enc = AES.new(key[:16], AES.MODE_CFB, iv)
            inp = enc.decrypt(inp[16:])
            key = key[16:]
        inp = _unpad(inp)
        return inp
