import asyncio

from composure.service.core.transport import Server, Client

from composure.service.base import Service
from composure.service.core.transport.handshake import Handshake


class TransportService(Service):
    ID = 'Transport'
    __logname__ = 'service.Transport'

    def init(self):
        self.transport = None
        self.transport_task = None
        self.connected_serde = []

        conf = self.config
        if conf['type'] == 'server':
            transport = self.create_server(conf)
        elif conf['type'] == 'client':
            transport = self.create_client(conf)
        else:
            return
        self.transport = transport

    @asyncio.coroutine
    def deinit(self):
        yield from self.transport.close()

    @asyncio.coroutine
    def run(self):
        self.transport_task = self.loop.create_task(self.transport.run())
        yield from self.transport_task

    def create_server(self, config):
        transport = Server(config=config, loop=self.loop)
        transport.register_callback('on_connection_made',
            self.on_connection_made_server(config))
        transport.register_callback('on_connection_lost',
            self.on_connection_lost_server(config))
        return transport

    def create_client(self, config):
        transport = Client(config=config, loop=self.loop)
        transport.register_callback('on_connection_made',
            self.on_connection_made_client(config))
        transport.register_callback('on_connection_lost',
            self.on_connection_lost_client(config))
        return transport

    @asyncio.coroutine
    def do_handshake(self, config, serde, server=False):
        hs_outputs = {}
        for hs in config['handshake']:
            hs_factory = Handshake.get_class(next(iter(hs.keys())))
            hs = hs_factory(
                serde=serde,
                config=next(iter(hs.values())).get('config'),
                outputs=hs_outputs,
                is_server=server)
            try:
                result = yield from hs.do()
                if not result.pop(Handshake.STR_STATUS):
                    break
            except Exception as e:
                print('ERROR', e.__class__, e)
                break
        return hs_outputs

    def on_connection_made_server(self, config):
        @asyncio.coroutine
        def _on_connection(serde):
            result = yield from self.do_handshake(config, serde, server=True)
            self.connected_serde.append(serde)
        return _on_connection

    def on_connection_made_client(self, config):
        @asyncio.coroutine
        def _on_connection(transport, serde):
            result = yield from self.do_handshake(config, serde, server=False)
            self.connected_serde.append(serde)
        return _on_connection

    def on_connection_lost_server(self, config):
        @asyncio.coroutine
        def _on_connection_lost(serde):
            self.connected_serde.remove(serde)
        return _on_connection_lost

    def on_connection_lost_client(self, config):
        @asyncio.coroutine
        def _on_connection_lost(transport, serde):
            self.connected_serde.remove(serde)
            if not transport.deinited:
                yield from transport.deinit()

        return _on_connection_lost
