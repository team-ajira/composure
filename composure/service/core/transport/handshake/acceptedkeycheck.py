from composure.utils.log import MessageLoader

from .base import Handshake
from .identity import IdentityExchange
import asyncio

messages = MessageLoader.load('composure.service.core.transport.handshake')


class AcceptedKeyCheck(Handshake):
    """
    Master to verify if the client key is accepted
    """

    ID = 'AcceptedKeyCheck'

    __logname__ = 'handshake.AcceptedKeyCheck'

    STR_ACCEPT = b'accept'

    CONFIG_CLIENTS = 'clients'
    CONFIG_KEY = 'key'
    CONFIG_ID = 'id'

    def strip_key(self, key):
        return key.replace('\r', '').replace('\n', '').replace(' ', '').replace('\t', '')

    @asyncio.coroutine
    def do_server(self):
        client_key = self.outputs[IdentityExchange.STR_KEY]

        # SerDe might send bytes instead of str
        if type(client_key) is not str:
            client_key = client_key.decode('ascii')

        client_key = self.strip_key(client_key)
        found = False
        for client in self.config[self.CONFIG_CLIENTS]:
            key = self.strip_key(client[self.CONFIG_KEY])
            if key == client_key:
                if client[self.CONFIG_ID] == self.outputs[IdentityExchange.STR_ID]:
                    found = True
                    break
        yield from self.serde.send({ self.STR_ACCEPT: found })
        if found:
            self.logger.info(messages.get('SERVER_ACCEPTED_KEY'))
        else:
            self.logger.warn(messages.get('SERVER_REJECTED_KEY'))
        return { self.STR_STATUS: found }

    @asyncio.coroutine
    def do_client(self):
        message = yield from self.serde.receive()

        accept = message[self.STR_ACCEPT]
        if accept:
            self.logger.info(messages.get('SERVER_ACCEPTED_KEY'))
        else:
            self.logger.warn(messages.get('SERVER_REJECTED_KEY'))
        return { self.STR_STATUS: accept }
