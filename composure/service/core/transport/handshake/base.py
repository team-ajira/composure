import asyncio
import logging

from composure.utils.factory import FactoryBase


class Handshake(FactoryBase):
    """
    Base class for handshake routines
    Derived class must implement do_server and do_client async methods
    performing appropriate handshake
    They must return a dictionary containing key Handshake.STR_STATUS
    """
    FACTORY_BASE = True

    STR_STATUS = b'status'

    __logname__ = 'handshake.Base'

    def __init__(self, serde, config, outputs=None, is_server=False):
        """
        Constructor
        :param serde: SerDe for performing handshake
        :param config: Configuration to be used
        :param outputs: Outputs from previous handshake
        :param is_server: indicate whether this is server. False implies client.
        """
        self.serde = serde
        self.outputs = outputs
        self.config = config
        self.is_server = is_server
        self.logger = logging.getLogger(self.__logname__)
        self.encoding = 'utf8'

    @asyncio.coroutine
    def do(self):
        """
        Perform the handshake
        :return: Handshake results (dict)
        """
        if self.is_server:
            result = yield from self.do_server()
            self.outputs.update(result)
            return self.outputs
        else:
            result = yield from self.do_client()
            self.outputs.update(result)
            return self.outputs

    @asyncio.coroutine
    def do_server(self):
        raise NotImplementedError()

    @asyncio.coroutine
    def do_client(self):
        raise NotImplementedError()
