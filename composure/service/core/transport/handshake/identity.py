from composure.utils.log import MessageLoader

from .base import Handshake

import asyncio

messages = MessageLoader.load('composure.service.core.transport.handshake')


class IdentityExchange(Handshake):
    """
    Server and client exchange identity information.
    """

    ID = 'IdentityExchange'
    __logname__ = 'handshake.IdentityExchange'

    STR_ID = b'id'
    STR_TYPE = b'type'
    STR_KEY = b'key'

    CONFIG_ID = 'id'
    CONFIG_TYPE = 'type'
    CONFIG_KEY = 'key'
    CONFIG_PUBLIC = 'public'
    CONFIG_PRIVATE = 'private'

    @asyncio.coroutine
    def send_identity(self):
        message = {
            self.STR_ID: self.config[self.CONFIG_ID],
            self.STR_TYPE: self.config[self.CONFIG_TYPE],
            self.STR_KEY: self.config[self.CONFIG_KEY][self.CONFIG_PUBLIC],
        }
        yield from self.serde.send(message)

    @asyncio.coroutine
    def receive_identity(self):
        message = yield from self.serde.receive()

        if message is None:
            return None

        return message

    @asyncio.coroutine
    def do_server(self):
        yield from self.send_identity()
        message = yield from self.receive_identity()
        self.logger.info(messages.get('IDENTITY_EXCHANGE_COMPLETE',
            name=message[self.STR_ID],
            type=message[self.STR_TYPE]
        ))
        return {
            self.STR_STATUS: True,
            self.STR_ID: message[self.STR_ID],
            self.STR_TYPE: message[self.STR_TYPE],
            self.STR_KEY: message[self.STR_KEY],
        }

    @asyncio.coroutine
    def do_client(self):
        message = yield from self.receive_identity()
        yield from self.send_identity()
        self.logger.info(messages.get('IDENTITY_EXCHANGE_COMPLETE',
            name=message[self.STR_ID],
            type=message[self.STR_TYPE]
        ))
        return {
            self.STR_STATUS: True,
            self.STR_ID: message[self.STR_ID],
            self.STR_TYPE: message[self.STR_TYPE],
            self.STR_KEY: message[self.STR_KEY],
        }
