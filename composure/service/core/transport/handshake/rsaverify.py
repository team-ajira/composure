from composure.utils.log import MessageLoader

from Crypto.PublicKey import RSA
from Crypto.Random import random

from .base import Handshake
from .identity import IdentityExchange

import asyncio

messages = MessageLoader.load('composure.service.core.transport.handshake')

STR_ENCODING = 'ascii'


"""
Steps server uses to verify client's public key

1. Server sends encrypted random challenge with the client's public key
2. Client decrypts using it's private key
3. Client reverses the decrypted challenge key
4. Client encrypts the reversed challenge using server's public key
5. Server decrypts using it's private key
6. Server verifies if the challenge was returned in reverse

Client verifies server's public key in a similar way
"""


class RSAVerification(Handshake):
    """
    Performs RSA public key verification of the counterpart.
    Generates and exchanges session key to be used for encryption
    """
    ID = 'RSAVerification'

    __logname__ = 'handshake.RSAVerification'

    STR_CHALLENGE = b'chlng'
    STR_RESPONSE = b'resp'
    STR_RESULT = b'res'
    STR_SESSION_KEY = b'sesskey'

    CONFIG_KEY = 'key'
    CONFIG_PUBLIC = 'public'
    CONFIG_PRIVATE = 'private'

    DEFAULT_CHAR_SET = '`1234567890-=~!@#$%^&*()_+qwertyuiop[]asdfghjkl;\'\\zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:"|ZXCVBNM<>?'

    CHALLENGE_LEN = 64
    CHALLENGE_SET = DEFAULT_CHAR_SET
    SESSION_KEY_LEN = 128
    SESSION_KEY_SET = DEFAULT_CHAR_SET

    def _get_challenge(self):
        s = ''
        for i in range(self.CHALLENGE_LEN):
            s += random.choice(self.CHALLENGE_SET)
        return s

    def _get_session_key(self):
        s = ''
        for i in range(self.SESSION_KEY_LEN):
            s += random.choice(self.SESSION_KEY_SET)
        return s

    def init(self):
        try:
            self.public_key = RSA.importKey(
                self.config[self.CONFIG_KEY][self.CONFIG_PUBLIC].encode(STR_ENCODING))
        except Exception as e:
            self.logger.error(messages.get('KEY_LOAD_FAILED',
                key_type='Public',
                exception_class=e.__class__,
                exception_message=str(e)
            ))
            return
        try:
            self.private_key = RSA.importKey(
                self.config[self.CONFIG_KEY][self.CONFIG_PRIVATE].encode(STR_ENCODING))
        except Exception as e:
            self.logger.error(messages.get('KEY_LOAD_FAILED',
                key_type='Private',
                exception_class=e.__class__,
                exception_message=str(e)
            ))
            return
        try:
            self.client_key = RSA.importKey(self.outputs[IdentityExchange.STR_KEY])
        except Exception as e:
            self.logger.error(messages.get('KEY_LOAD_FAILED',
                key_type='Client',
                exception_class=e.__class__,
                exception_message=str(e)
            ))
            return
        self.logger.info(messages.get('RSA_KEYS_LOAD_SUCCESS'))

    def _encode(self, inp, key):
        inp = inp.encode(STR_ENCODING)
        inp = key.encrypt(inp, 32)[0]
        return inp

    def _decode(self, inp, key):
        inp = key.decrypt(inp)
        inp = inp.decode(STR_ENCODING)
        return inp

    @asyncio.coroutine
    def challenge(self):
        challenge = self._get_challenge()
        message = {
            self.STR_CHALLENGE: self._encode(
                challenge, self.client_key),
        }
        yield from self.serde.send(message)
        message = yield from self.serde.receive()
        response = self._decode(
            message[self.STR_RESPONSE], self.private_key)
        res = challenge == response[::-1]
        yield from self.serde.send({ self.STR_RESULT: res })
        return res

    @asyncio.coroutine
    def respond(self):
        message = yield from self.serde.receive()
        challenge = self._decode(message[self.STR_CHALLENGE], self.private_key)
        challenge = challenge[::-1]
        message = {
            self.STR_RESPONSE: self._encode(challenge, self.client_key)
        }
        yield from self.serde.send(message)
        result = yield from self.serde.receive()
        return result[self.STR_RESULT]

    @asyncio.coroutine
    def send_session_key(self, session_key):
        message = {
            self.STR_SESSION_KEY: self._encode(session_key, self.client_key)
        }
        yield from self.serde.send(message)

    @asyncio.coroutine
    def receive_session_key(self):
        message = yield from self.serde.receive()
        return self._decode(
            message[self.STR_SESSION_KEY], self.private_key)

    @asyncio.coroutine
    def do_server(self):
        self.init()

        res = yield from self.challenge()
        if not res:
            return { self.STR_STATUS: False }
        res = yield from self.respond()
        if not res:
            return { self.STR_STATUS: False }

        self.logger.info(messages.get('RSA_VERIFY_COMPLETE'))
        session_key = self._get_session_key()
        yield from self.send_session_key(session_key)
        self.logger.info(messages.get('SESS_KEY_EXCHANGE_COMPLETE'))
        self.serde.set_session_key(session_key)

        return {
            self.STR_STATUS: True,
            self.STR_SESSION_KEY: session_key
        }

    @asyncio.coroutine
    def do_client(self):
        self.init()

        res = yield from self.respond()
        if not res:
            return { self.STR_STATUS: False }
        res = yield from self.challenge()
        if not res:
            return { self.STR_STATUS: False }

        self.logger.info(messages.get('RSA_VERIFY_COMPLETE'))
        session_key = yield from self.receive_session_key()
        self.logger.info(messages.get('SESS_KEY_EXCHANGE_COMPLETE'))
        self.serde.set_session_key(session_key)

        return {
            self.STR_STATUS: True,
            self.STR_SESSION_KEY: session_key
        }
