import asyncio

from composure.service.core.transport.base import Transport
from composure.service.core.transport.serde import SerDe

from composure.helper.callback import CallbackHelper


class Client(Transport, CallbackHelper):
    """
    Client transport. Connects to server.
    """

    __logname__ = 'transport.Client'

    def init(self):
        """
        Client initialization
        """
        self.callback_init() # Needed for CallbackHelper

        self.server_address = (self.config['server']['host'],
            self.config['server']['port'])

        self.serde_factory = SerDe.get_class(self.config['serde'])

        self.retry_attempts = self.config['retry']['attempts']
        self.retry_interval = self.config['retry']['interval']

        self.serde = None
        self.serde_task = None
        # There is no need to close the protocol here because, protocol run
        # is either complete by themselves, or finished via deinit. In either
        # case, connection must be correctly closed internally.
        self.complete = False

    def reinit(self):
        self.serde = None
        self.serde_task = None

    @asyncio.coroutine
    def _handle(self, reader, writer):
        self.serde = self.serde_factory(reader, writer, server=False)
        self.serde_task = self.loop.create_task(self.serde.run())
        yield from self.do_callback('on_connection_made', self, self.serde)
        yield from self.serde_task
        yield from self.do_callback('on_connection_lost', self, self.serde)

    @asyncio.coroutine
    def deinit(self):
        """
        Client de-initialization
        """
        # Close the serde and wait for the task to complete
        self.deinited = True

        if self.serde is not None:
            yield from self.serde.close()
        if self.serde_task is not None:
            yield from self.serde_task

        self.serde = None
        self.serde_task = None

    @asyncio.coroutine
    def close(self):
        """
        Close the client
        """
        self.logger.info('Closing client...')
        self.complete = True

        if not self.deinited:
            yield from self.deinit()

    @asyncio.coroutine
    def run(self):
        """
        Run loop for the client
        """
        self.deinited = False
        self.complete = False

        server_address = self.server_address
        connected = False
        while not self.complete:
            for i in range(self.retry_attempts): # TODO: User proper config
                try:
                    # Check if cancelled before successful connect
                    if self.complete:
                        return

                    if self.deinited:
                        self.reinit()

                    self.logger.info('Connecting to master on %s' % str(self.server_address))
                    reader, writer = yield from asyncio.open_connection(
                        server_address[0], server_address[1], loop=self.loop)
                    connected = True
                    self.logger.info('Connected successfully')
                    yield from self._handle(reader, writer)
                    self.complete = True
                except Exception as e:
                    self.logger.warn('Unable to connect. Will retry...')
                    yield from asyncio.sleep(self.retry_interval)
            if not connected:
                if not self.complete:
                    self.logger.error('Retry timeout!')
