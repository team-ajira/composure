from composure.service.base import Service

from aiohttp import web

import asyncio


class HttpService(Service):
    ID = 'Http'
    __logname__ = 'service.Http'

    def init(self):
        self.transport = None
        self.transport_task = None
        self.connected_serde = []

        self.app = web.Application(loop=self.loop)
        self.router = self.app.router

        self.listen_address = (self.config['listen']['host'],
                               self.config['listen']['port'])

    @asyncio.coroutine
    def deinit(self):
        self.running.set_result(None)
        self.server.close()

    @asyncio.coroutine
    def run(self):
        loop = self.loop
        self.running = asyncio.Future()
        handler = self.app.make_handler()
        self.server = loop.create_server(handler, self.listen_address[0], self.listen_address[1], ssl=None,
                                backlog=5)
        yield from self.server
        yield from self.running

    def register_endpoint(self, method='get', route=None, handler=None):
        if method == 'get':
            self.app.router.add_get(route, handler)
