import asyncio
import logging


class EventManager(object):
    """
    Manages events registration and dispatch
    """
    def __init__(self, loop=None):
        self._routes = {}
        self.loop = loop or asyncio.get_event_loop()

    def subscribe(self, service_id, event_name, callback):
        """
        Subscribe for an event from a service
        :param service_id: id of the service
        :param event_name: event name
        :param callback: callback to be executed
        """
        self._routes.setdefault((service_id, event_name), []).append(callback)

    @asyncio.coroutine
    def dispatch(self, service_id, event_name, *args, **kwargs):
        """
        Dispatch the event
        :param service_id: originating service id
        :param event_name: event name
        :param args: positional args to the callback
        :param kwargs: keyword args to the callback
        """
        # TODO: This approach still blocks the caller. Find an alternative
        # TODO: Allow patterns on service id and event names
        for cb in self._routes.get((service_id, event_name), []):
            if asyncio.iscoroutinefunction(cb):
                yield from cb(*args, **kwargs)
            else:
                cb(*args, **kwargs)


class ServiceManager(object):
    """
    Manager for all the services in the process
    """
    def __init__(self, loop=None):
        self.loop = loop or asyncio.get_event_loop()
        self.logger = logging.getLogger('service.Manager')
        self._services = {}
        self._tasks = []
        self.events = EventManager(loop=self.loop)

    def register(self, name, service):
        """
        Register a service with the manager
        :param name: service name
        :param service: service instance
        """
        service.__manager__ = self
        self._services[name] = service

    def get_service(self, name):
        """
        Get service by name
        :param name: service name
        :return: service instance
        """
        return self._services.get(name, None)

    @asyncio.coroutine
    def startup(self):
        """
        Starts task for all the services
        """
        self.logger.info('Starting up...')
        for service in self._services.values():
            task = self.loop.create_task(service.run())
            self._tasks.append(task)

    @asyncio.coroutine
    def shutdown(self):
        """
        De-inits all the services and wait for them to complete
        """
        self.logger.info('Shutting down...')
        for service in self._services.values():
            yield from service.deinit()

        for task in self._tasks:
            yield from task
