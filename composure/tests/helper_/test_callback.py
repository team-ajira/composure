from composure.helper.callback import CallbackHelper

from composure.tests.helper import AioTestCase
import asyncio


@asyncio.coroutine
def cb1(self):
    self.cb1_res = True


def cb2(self):
    self.cb2_res = True


class TestCallback(AioTestCase):
    @asyncio.coroutine
    def test_callback(self):
        class A(CallbackHelper):
            pass

        a = A()
        a.callback_init()
        self.cb1_res = False
        self.cb2_res = False

        a.register_callback('cb1', cb1)
        a.register_callback('cb2', cb2)

        yield from a.do_callback('cb1', self)
        yield from a.do_callback('cb2', self)

        self.assertTrue(self.cb1_res)
        self.assertTrue(self.cb2_res)
