from composure.service import Service

# Just for registry
import composure.service.core

from composure.tests.helper import AioTestCase

import asyncio


class TransportServiceTest(AioTestCase):
    SERVER_CONFIG = {
        'type': 'server',
        'serde': 'Pickle',
        'handshake': [],
        'listen': {
            'host': '127.0.0.1',
            'port': 1880,
        },
    }

    CLIENT_CONFIG = {
        'type': 'client',
        'serde': 'Pickle',
        'handshake': [],
        'server': {
            'host': '127.0.0.1',
            'port': 1880,
        },
        'retry': {
            'attempts': 10,
            'interval': 0.1
        },
    }

    def setUp(self):
        self.server = Service.get_class('Transport')(config=self.SERVER_CONFIG, loop=self.loop)
        self.client = Service.get_class('Transport')(config=self.CLIENT_CONFIG, loop=self.loop)

    @asyncio.coroutine
    def test_transport(self):
        self.t1 = self.loop.create_task(self.server.run())
        self.t2 = self.loop.create_task(self.client.run())

    @asyncio.coroutine
    def tearDown(self):
        yield from asyncio.sleep(1)
        yield from self.server.deinit()
        yield from self.client.deinit()
        yield from self.t1
