from composure.service import Service

# Just for registry
import composure.service.core

from composure.tests.helper import AioTestCase

from aiohttp import web
from aiohttp import ClientSession

import asyncio


class TransportServiceTest(AioTestCase):
    SERVER_CONFIG = {
        'listen': {
            'host': '0.0.0.0',
            'port': 8080,
        },
    }

    @asyncio.coroutine
    def hello_get(self, request):
        return web.Response(text='Hello')

    def setUp(self):
        self.server = Service.get_class('Http')(config=self.SERVER_CONFIG, loop=self.loop)
        self.client = ClientSession()

    @asyncio.coroutine
    def test_get(self):
        self.server.register_endpoint('get', '/', self.hello_get)
        self.t1 = self.loop.create_task(self.server.run())
        yield from asyncio.sleep(0.1)
        resp = yield from self.client.get('http://127.0.0.1:8080/')
        text = yield from resp.text()
        self.assertEqual(text, 'Hello')

    @asyncio.coroutine
    def tearDown(self):
        yield from asyncio.sleep(0.1)
        yield from self.client.close()
        yield from self.server.deinit()
        yield from self.t1
