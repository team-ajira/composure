import asyncio

from composure.service.core.transport.serde import SerDe
from composure.tests.helper import AioTestCase
from composure.tests.transport.helper import Writer

try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    pass


DATA_SET = [
    b'Hello',
    100,
    222.5,
    [1, 2.5, b'a'],
    {b'Hello': b'World'},
]

MsgPack = SerDe.get_class('MsgPack')
Pickle = SerDe.get_class('Pickle')


class MsgPackSerdeTest(AioTestCase):
    @asyncio.coroutine
    def test_basic(self):
        loop = self.loop

        sr1 = asyncio.StreamReader()
        sr2 = asyncio.StreamReader()

        sw1 = Writer(sr2)
        sw2 = Writer(sr1)

        serde1 = MsgPack(sr1, sw1)
        serde2 = MsgPack(sr2, sw2)

        @asyncio.coroutine
        def send_message():
            for data in DATA_SET:
                yield from serde1.send(data)
                res = yield from serde2.receive()
                self.assertEqual(res, data)
            yield from serde2.close()
            yield from serde1.close()

        t1 = loop.create_task(serde1.run())
        t2 = loop.create_task(serde2.run())
        t3 = loop.create_task(send_message())

        yield from t1
        yield from t2
        yield from t3

    @asyncio.coroutine
    def test_secure(self):
        loop = self.loop

        sr1 = asyncio.StreamReader()
        sr2 = asyncio.StreamReader()

        sw1 = Writer(sr2)
        sw2 = Writer(sr1)

        serde1 = MsgPack(sr1, sw1)
        serde2 = MsgPack(sr2, sw2)

        serde1.set_session_key('qwertyuiasdfghjk')
        serde2.set_session_key('qwertyuiasdfghjk')

        @asyncio.coroutine
        def send_message():
            for data in DATA_SET:
                yield from serde1.send(data, secure=True)
                res = yield from serde2.receive()
                self.assertEqual(res, data)
            yield from serde2.close()
            yield from serde1.close()

        t1 = loop.create_task(serde1.run())
        t2 = loop.create_task(serde2.run())
        t3 = loop.create_task(send_message())

        yield from t1
        yield from t2
        yield from t3


class PickleSerdeTest(AioTestCase):
    @asyncio.coroutine
    def test_basic(self):
        loop = loop = self.loop

        sr1 = asyncio.StreamReader(loop=loop)
        sr2 = asyncio.StreamReader(loop=loop)

        sw1 = Writer(sr2)
        sw2 = Writer(sr1)

        serde1 = Pickle(sr1, sw1)
        serde2 = Pickle(sr2, sw2)

        @asyncio.coroutine
        def send_message():
            for data in DATA_SET:
                yield from serde1.send(data)
                res = yield from serde2.receive()
                self.assertEqual(res, data)
            yield from serde1.close()
            yield from serde2.close()

        t1 = loop.create_task(serde1.run())
        t2 = loop.create_task(serde2.run())
        t3 = loop.create_task(send_message())

        yield from t1
        yield from t2
        yield from t3

    @asyncio.coroutine
    def test_secure(self):
        loop = self.loop

        sr1 = asyncio.StreamReader()
        sr2 = asyncio.StreamReader()

        sw1 = Writer(sr2)
        sw2 = Writer(sr1)

        serde1 = Pickle(sr1, sw1)
        serde2 = Pickle(sr2, sw2)

        serde1.set_session_key('qwertyuiasdfghjk')
        serde2.set_session_key('qwertyuiasdfghjk')

        @asyncio.coroutine
        def send_message():
            for data in DATA_SET:
                yield from serde1.send(data, secure=True)
                res = yield from serde2.receive()
                self.assertEqual(res, data)
            yield from serde2.close()
            yield from serde1.close()

        t1 = loop.create_task(serde1.run())
        t2 = loop.create_task(serde2.run())
        t3 = loop.create_task(send_message())

        yield from t1
        yield from t2
        yield from t3
