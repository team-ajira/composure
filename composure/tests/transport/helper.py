import asyncio


class Writer(object):
    def __init__(self, reader):
        self.data = b''
        self.reader = reader

    def write(self, data):
        if data is not None:
            self.data += data

    @asyncio.coroutine
    def drain(self):
        self.reader.feed_data(self.data)
        self.data = b''

    def close(self):
        if self.data != b'':
            self.reader.feed_data(self.data)
        self.reader.feed_eof()
