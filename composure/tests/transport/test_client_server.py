import asyncio

from composure.service.core.transport import Server, Client
from composure.tests.helper import AioTestCase

try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except:
    pass


class ClientServerTest(AioTestCase):
    @asyncio.coroutine
    def test_client_server1(self):
        loop = asyncio.get_event_loop()
        SERVER_CONFIG = {
            'listen': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack'
        }

        CLIENT_CONFIG = {
            'server': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack',
            'retry': {
                'attempts': 100,
                'interval': 0.01
            }
        }
        server = Server(SERVER_CONFIG, loop=loop)
        client = Client(CLIENT_CONFIG, loop=loop)

        @asyncio.coroutine
        def connection_made(transport, serde):
            loop.create_task(client.close())
            loop.create_task(server.close())

        client.register_callback('on_connection_made', connection_made)

        s_task = loop.create_task(server.run())
        c_task = loop.create_task(client.run())

        yield from s_task
        yield from c_task


    @asyncio.coroutine
    def test_client_server2(self):
        loop = asyncio.get_event_loop()
        SERVER_CONFIG = {
            'listen': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack'
        }

        CLIENT_CONFIG = {
            'server': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack',
            'retry': {
                'attempts': 100,
                'interval': 0.01
            }
        }
        server = Server(SERVER_CONFIG, loop=loop)
        client = Client(CLIENT_CONFIG, loop=loop)

        @asyncio.coroutine
        def connection_made(transport, serde):
            loop.create_task(server.close())
            loop.create_task(client.close())

        client.register_callback('on_connection_made', connection_made)

        s_task = loop.create_task(server.run())
        c_task = loop.create_task(client.run())

        yield from s_task
        yield from c_task

    @asyncio.coroutine
    def test_multi_client_server1(self):
        loop = asyncio.get_event_loop()
        SERVER_CONFIG = {
            'listen': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack'
        }

        CLIENT_CONFIG = {
            'server': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack',
            'retry': {
                'attempts': 100,
                'interval': 0.01
            }
        }
        server = Server(SERVER_CONFIG, loop=loop)
        client1 = Client(CLIENT_CONFIG, loop=loop)
        client2 = Client(CLIENT_CONFIG, loop=loop)

        self.count = 0

        @asyncio.coroutine
        def connection_made(transport, serde):
            self.count += 1
            if self.count == 2:
                loop.create_task(server.close())
                loop.create_task(client1.close())
                loop.create_task(client2.close())

        client1.register_callback('on_connection_made', connection_made)
        client2.register_callback('on_connection_made', connection_made)

        s_task = loop.create_task(server.run())
        c1_task = loop.create_task(client1.run())
        c2_task = loop.create_task(client2.run())

        yield from s_task
        yield from c1_task
        yield from c2_task

    @asyncio.coroutine
    def test_multi_client_server2(self):
        loop = asyncio.get_event_loop()
        SERVER_CONFIG = {
            'listen': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack'
        }

        CLIENT_CONFIG = {
            'server': {
                'host': '127.0.0.1',
                'port': 1880,
            },
            'serde': 'MsgPack',
            'retry': {
                'attempts': 100,
                'interval': 0.01
            }
        }
        server = Server(SERVER_CONFIG, loop=loop)
        client1 = Client(CLIENT_CONFIG, loop=loop)
        client2 = Client(CLIENT_CONFIG, loop=loop)

        self.count = 0

        @asyncio.coroutine
        def connection_made(transport, serde):
            self.count += 1
            if self.count == 2:
                loop.create_task(client1.close())
                loop.create_task(client2.close())
                loop.create_task(server.close())

        client1.register_callback('on_connection_made', connection_made)
        client2.register_callback('on_connection_made', connection_made)

        s_task = loop.create_task(server.run())
        c1_task = loop.create_task(client1.run())
        c2_task = loop.create_task(client2.run())

        yield from s_task
        yield from c1_task
        yield from c2_task
