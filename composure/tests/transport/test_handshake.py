import asyncio

from composure.service.core.transport.serde import MsgPack

from composure.service.core.transport.handshake import IdentityExchange, AcceptedKeyCheck, RSAVerification
from composure.tests.helper import AioTestCase
from composure.tests.transport.helper import Writer

try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except:
    pass


class HandshakeTest(AioTestCase):
    def setUp(self):
        from Crypto.PublicKey import RSA
        key1 = RSA.generate(1024)
        key2 = RSA.generate(1024)

        self.SERVER_IDENTITY = {
            'id': 'server',
            'type': 'server',
            'key': {
                'public': key1.publickey().exportKey().decode('ascii'),
                'private': key1.exportKey().decode('ascii')
            }
        }
        self.CLIENT_IDENTITY = {
            'id': 'client',
            'type': 'client',
            'key': {
                'public': key2.publickey().exportKey().decode('ascii'),
                'private': key2.exportKey().decode('ascii')
            }
        }
        self.SERVER_ACCEPT = {
            'clients': [
                {
                    'id': 'client',
                    'key': key2.publickey().exportKey().decode('ascii'),
                },
            ]
        }
        self.HS_SEQ_SERVER = [
            (IdentityExchange, self.SERVER_IDENTITY),
            (AcceptedKeyCheck, self.SERVER_ACCEPT),
            (RSAVerification, self.SERVER_IDENTITY),
        ]

        self.HS_SEQ_CLIENT = [
            (IdentityExchange, self.CLIENT_IDENTITY),
            (AcceptedKeyCheck, {}),
            (RSAVerification, self.CLIENT_IDENTITY),
        ]

    @asyncio.coroutine
    def test_all(self):
        loop = self.loop

        sr1 = asyncio.StreamReader()
        sr2 = asyncio.StreamReader()

        sw1 = Writer(sr2)
        sw2 = Writer(sr1)

        serde1 = MsgPack(sr1, sw1)
        serde2 = MsgPack(sr2, sw2)

        @asyncio.coroutine
        def perform_handshake_server():
            outputs = {}
            for hs_factory, config in self.HS_SEQ_SERVER:
                hs = hs_factory(
                    serde=serde1,
                    config=config,
                    outputs=outputs,
                    is_server=True
                )
                result = yield from hs.do()
                self.assertTrue(result[b'status'])

        @asyncio.coroutine
        def perform_handshake_client():
            outputs = {}
            for hs_factory, config in self.HS_SEQ_CLIENT:
                hs = hs_factory(
                    serde=serde2,
                    config=config,
                    outputs=outputs,
                    is_server=False
                )
                result = yield from hs.do()
                self.assertTrue(result[b'status'])

        @asyncio.coroutine
        def close():
            yield from serde2.close()
            yield from serde1.close()

        t1 = loop.create_task(serde1.run())
        t2 = loop.create_task(serde2.run())
        t3 = loop.create_task(perform_handshake_server())
        t4 = loop.create_task(perform_handshake_client())

        yield from t3
        yield from t4
        yield from loop.create_task(close())
        yield from t1
        yield from t2
