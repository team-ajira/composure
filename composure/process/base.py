import asyncio
import logging


class Process(object):
    """
    Base class for composure processes
    """
    CURRENT_PROCESS = None
    __logname__ = 'composure.Base'

    def __init__(self, **extra):
        self.loop = asyncio.get_event_loop()

        self.extra = extra
        logging.basicConfig(level=logging.INFO, format='%(asctime)-25s %(levelname)-8s %(name)-32s %(threadName)-12s %(message)s')
        self.logger = logging.getLogger(self.__logname__)

        self.running = False
        self.encryptor = None
        self.closed = False

        self.init()

        self.CURRENT_PROCESS = self

    def init(self):
        """
        To be defined in derived class
        """
        raise NotImplementedError()

    @asyncio.coroutine
    def deinit(self):
        """
        To be defined in derived class
        """
        raise NotImplementedError()

    def run(self):
        """
        Run loop for the process
        """
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            print('Exiting...')
            self.loop.run_until_complete(self.deinit())
            self.loop.close()
        except Exception as e:
            self.logger.error('Could not start the process, {: %s :} {: %s :}' % (e.__class__.__name__, str(e)))
        self.logger.info('Run complete')

    @classmethod
    def get_current_process(cls):
        """
        Get current process instance
        :return: Process instance
        """
        return cls.CURRENT_PROCESS
