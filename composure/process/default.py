from composure.process.base import Process

from composure.service.base import Service
from composure.service.manager import ServiceManager
from composure.utils.config import Config

import argparse
import asyncio
import importlib

try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except:
    pass


class ComposureProcess(Process):
    __logname__ = 'composure.Process'
    
    def parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--conf', help='Confguration file',
            action='store')
        options = parser.parse_args()

        self.logger.info('Reading configuration from {: %s :}' % options.conf)
        Config.load(options.conf)

    def load_modules(self):
        for m in Config.value('factory_modules'):
            importlib.import_module(m)

    def load_service(self, sdef):
        sf = Service.get_class(sdef.get('class'))
        if sf is None:
            raise NameError('Service class not found')
        service = sf(config=sdef.get('config'), loop=self.loop)
        service.__id__ = sdef.get('id')
        return service

    def load_services(self):
        self.service_manager = ServiceManager(loop=self.loop)
        for service_config in Config.value('services'):
            self.service_manager.register(
                service_config.get('id'),
                self.load_service(service_config)
            )

    def init(self):
        self.parse_args()
        self.load_modules()
        self.load_services()
        self.loop.create_task(self.service_manager.startup())

    @asyncio.coroutine
    def deinit(self):
        yield from self.service_manager.shutdown()


def main():
    ComposureProcess().run()


if __name__ == '__main__':
    main()
